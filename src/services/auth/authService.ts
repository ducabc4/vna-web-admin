import MockAdapter from 'axios-mock-adapter'
import { http } from 'http/index'

const mock = new MockAdapter(http)

mock.onPost('/login').reply(200, {
  user: {
    name: 'Duc',
    id: 1,
    isFirstTimeLogin: true
  },
  token: '123123123123'
})
export class AuthService {
  login(username: string, password: string): any {
    return http.post('/login', {
      username,
      password
    })
  }

  resetPassword({ email, phoneNumber }: { email?: string; phoneNumber?: string }): any {
    return http.post('/reset-password', {
      email,
      phoneNumber
    })
  }

  changePassword({ newPassword, confirmPassword }: { newPassword: string; confirmPassword: string }): any {
    return http.post('/change-password', {
      newPassword,
      confirmPassword
    })
  }

  logout() {
    return http.post('/logout')
  }

  getNewJWTTokenIfExpired() {
    return ''
  }
}

export default new AuthService()
