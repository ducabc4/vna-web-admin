import { pick } from 'lodash'
import store from 'store'
import { setRouterConfig, addToHistories } from 'store/slices/router'

import { IRouterItem } from 'router/type'
import { IRouterDomLocation } from './type'

export class RouterService {
  handleAfterRouteEnter(currentRoute: IRouterItem, location: IRouterDomLocation): void {
    setTimeout((): void => {
      const item: any = pick(currentRoute, ['path', 'isPrivate', 'layout', 'name'])

      item.location = location
      store.dispatch(setRouterConfig(item))
    })
  }
}

export default new RouterService()
