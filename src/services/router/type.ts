import { IRouteConfigProps } from 'router/type'

export interface IRouterDomLocation {
  hash: string
  key: string
  pathname: string
  search: string
  state?: any | undefined
}

export interface IRouteProps {
  routeConfig: IRouteConfigProps
}
