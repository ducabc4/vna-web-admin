import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import routerSlice from './slices/router'
import userSlice from './slices/user'

const customizedMiddleware: any = getDefaultMiddleware({
  serializableCheck: false
})

const persistConfig: any = {
  key: 'root',
  storage,
  whitelist: ['user']
}

const persistedReducer: any = persistReducer(
  persistConfig,
  combineReducers({
    user: userSlice,
    router: routerSlice
  })
)

const store = configureStore({
  reducer: persistedReducer,
  middleware: customizedMiddleware
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch

export default store
