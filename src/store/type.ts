import { IUser } from 'services/user/type'
import { IRouterDomLocation } from 'services/router/type'
import { IRouterItem } from 'router/type'

export interface IUserStore {
  currentUser: IUser
}

export interface IRouterStore {
  current: IRouterItemHistory | null
  histories: IRouterItemHistory[]
}

export interface IStore {
  user: IUserStore
  router: IRouterStore
}

export interface IRouterItemHistory extends IRouterItem {
  location: IRouterDomLocation
}

export interface RouterStateType {
  current: IRouterItemHistory | null
  histories: IRouterItemHistory[]
}
