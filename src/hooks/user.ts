export interface IUseUserPermission {
  isHasPermission: boolean
}

export const useUserPermission = (): IUseUserPermission => {
  return {
    isHasPermission: true
  }
}

export const useCheckIsLogedIn = (): boolean => {
  return !!localStorage.getItem('token')
}
