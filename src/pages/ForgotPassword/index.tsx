import { PhoneOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Col, Form, Input, Radio, Row } from 'antd'
import { RadioChangeEvent } from 'antd/lib/radio'
import { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { ROUTER_PATH } from 'router/enum'
import authService from 'services/auth/authService'

import s from './forgotPassword.module.scss'

type TResetBy = 'byMail' | 'byPhone'

const ForgotPassword: FC = (): JSX.Element => {
  const { t } = useTranslation()
  const [resetBy, setResetBy] = useState<TResetBy>('byMail')
  const [loading, setLoading] = useState(false)

  const onFinish = (values: any) => {
    if (resetBy === 'byMail') {
      handleResetPass({ email: values.email })
    } else {
      handleResetPass({ phoneNumber: values.phoneNumber })
    }
  }

  const handleResetPass = (payload: { email?: string; phoneNumber?: string }): void => {
    setLoading(true)
    authService
      .resetPassword(payload)
      .then((res: any) => {
        //do something
        console.log(res)
      })
      .catch((err: any) => {
        //do something
        console.log(err)
      })
      .finally(() => setLoading(false))
  }

  const handleChangeResetBy = (e: RadioChangeEvent) => setResetBy(e.target.value)

  return (
    <div className={s.fpContainer}>
      <div className={s.fpTitle}>
        <h1>{t('forgotPasswordPage.title')}</h1>
      </div>
      <div className={s.formContainer}>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            resetMethod: resetBy,
            email: '',
            phoneNumber: ''
          }}
          onFinish={onFinish}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ width: '100%' }}
        >
          <Row style={{ marginBottom: 24 }}>
            <Col span={8} style={{ textAlign: 'right' }}>
              <span style={{ marginRight: 8, lineHeight: '32px' }}>{t('forgotPasswordPage.resetBy')}:</span>
            </Col>
            <Col span={16}>
              <Radio.Group onChange={handleChangeResetBy} defaultValue={resetBy}>
                <Radio.Button value="byMail">{t('forgotPasswordPage.byMail')}</Radio.Button>
                <Radio.Button value="byPhone">{t('forgotPasswordPage.byPhone')}</Radio.Button>
              </Radio.Group>
            </Col>
          </Row>

          <Form.Item
            name="email"
            rules={[{ required: resetBy === 'byMail', message: t('forgotPasswordPage.emailError'), type: 'email' }]}
            label={t('forgotPasswordPage.emailTitle')}
          >
            <Input
              prefix={<UserOutlined />}
              disabled={resetBy !== 'byMail'}
              placeholder={t('forgotPasswordPage.emailTitle')}
            />
          </Form.Item>
          <Form.Item
            name="phoneNumber"
            rules={[{ required: resetBy === 'byPhone', message: t('forgotPasswordPage.phoneError') }]}
            label={t('forgotPasswordPage.phoneTitle')}
          >
            <Input
              prefix={<PhoneOutlined />}
              disabled={resetBy !== 'byPhone'}
              placeholder={t('forgotPasswordPage.phoneTitle')}
            />
          </Form.Item>

          <Form.Item wrapperCol={{ span: 16, offset: 8 }}>
            <>
              <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                {t('forgotPasswordPage.btnTitle')}
              </Button>
              <Link to={ROUTER_PATH.LOGIN}>{t('forgotPasswordPage.backToLogin')}</Link>
            </>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}
export default ForgotPassword
