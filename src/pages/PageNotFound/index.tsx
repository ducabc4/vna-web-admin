import { FC } from 'react'
import { Link } from 'react-router-dom'

import s from './style.module.scss'

export const PageNotFound: FC = (): JSX.Element => {
  return (
    <div className={s.login}>
      <p>404 page</p>

      <ul>
        <li>
          <Link to="/">Go to Dashboard</Link>
        </li>
        <li>
          <Link to="/login">Go to Login</Link>
        </li>
        <li>
          <Link to="/duck-ngoxngex">Go to 404</Link>
        </li>
      </ul>
    </div>
  )
}

export default PageNotFound
