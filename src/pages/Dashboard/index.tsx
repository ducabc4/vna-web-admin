import { FC } from 'react'
import { Link } from 'react-router-dom'

import s from './style.module.scss'

export const Dashboard: FC = (): JSX.Element => {
  return (
    <div className={s.dashboard}>
      <p>Dashboard page</p>

      <ul>
        <li>
          <Link to="/">Go to Dashboard</Link>
        </li>
        <li>
          <Link to="/login">Go to Login</Link>
        </li>
        <li>
          <Link to="/duck-ngoxngex">Go to 404</Link>
        </li>
      </ul>

      <p>long content</p>
      {
        // indicates very long content
        Array.from({ length: 100 }, (_, index) => (
          <span key={index}>
            {index % 20 === 0 && index ? 'more' : '...'}
            <br />
          </span>
        ))
      }
    </div>
  )
}

export default Dashboard
