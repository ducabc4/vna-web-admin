import { LockOutlined } from '@ant-design/icons'
import { Button, Form, Input, notification, Typography } from 'antd'
import { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { ROUTER_PATH } from 'router/enum'
import { AuthService } from 'services/auth/authService'

import s from './firstTimeLogin.module.scss'

const FirstTimeLogin = () => {
  const { t } = useTranslation()
  const navigate = useNavigate()
  const [loading, setLoading] = useState(false)
  const onFinish = (values: any) => {
    const { newPassword, confirmPassword } = values
    setLoading(true)
    AuthService.prototype
      .changePassword({
        newPassword,
        confirmPassword
      })
      .then(() => {
        notification.success({ message: t('firstTimeLoginPage.notifyChangePasswordSuccess') })

        setTimeout(() => {
          navigate(ROUTER_PATH.LOGIN)
        }, 1000)
      })
      .catch((err: any) => {
        notification.error({ message: err.message })
      })
      .finally(() => {
        setLoading(false)
      })
  }
  return (
    <div className={s.firstTimeLogin}>
      <div className={s.loginTitle}>
        <Typography.Title level={1}>{t('firstTimeLoginPage.welcomeTitle')}</Typography.Title>
        <Typography.Title level={3}>{t('firstTimeLoginPage.subTitle')}</Typography.Title>
      </div>
      <div className={s.formContainer}>
        <Form
          name="first_time_login"
          initialValues={{
            newPassword: '',
            confirmPassword: ''
          }}
          onFinish={onFinish}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ width: '100%' }}
        >
          <Form.Item
            name="newPassword"
            rules={[{ required: true, message: t('firstTimeLoginPage.newPasswordError') }]}
            label={t('firstTimeLoginPage.newPasswordTitle')}
          >
            <Input.Password prefix={<LockOutlined />} placeholder={t('firstTimeLoginPage.newPasswordTitle')} />
          </Form.Item>
          <Form.Item
            name="confirmPassword"
            rules={[{ required: true, message: t('firstTimeLoginPage.confirmPasswordError') }]}
            label={t('firstTimeLoginPage.reEnterPasswordTitle')}
          >
            <Input.Password
              prefix={<LockOutlined />}
              type="password"
              placeholder={t('firstTimeLoginPage.reEnterPasswordTitle')}
            />
          </Form.Item>

          <Form.Item wrapperCol={{ span: 16, offset: 8 }}>
            <>
              <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                {t('firstTimeLoginPage.btnUpdate')}
              </Button>
            </>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default FirstTimeLogin
