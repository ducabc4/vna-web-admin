import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Checkbox, Form, Input } from 'antd'
import { useAppDispatch } from 'hooks/rtk'
import { FC, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Link, useNavigate } from 'react-router-dom'

import { ROUTER_PATH } from 'router/enum'
import { AuthService } from 'services/auth/authService'
import { setCurrentUser } from 'store/slices/user'
import s from './login.module.scss'

export const Login: FC = (): JSX.Element => {
  const { t } = useTranslation()
  const dispatch = useAppDispatch()
  const navigate = useNavigate()

  const [loading, setLoading] = useState(false)

  const onFinish = (values: any) => {
    setLoading(true)
    AuthService.prototype
      .login(values.usename, values.password)
      .then((res: any) => {
        const { data } = res

        if (data.user && data.token) {
          if (data.user.isFirstTimeLogin) {
            navigate(ROUTER_PATH.FIRST_TIME_LOGIN)
          } else {
            dispatch(setCurrentUser(res.data.user))
            localStorage.setItem('token', res.data.token)
            navigate(ROUTER_PATH.DASHBOARD)
          }
        }
      })
      .catch((err: any) => {
        //do something
        console.log(err)
      })
      .finally(() => {
        setLoading(false)
      })
  }

  return (
    <div className={s.loginContainer}>
      <div className={s.loginTitle}>
        <h1>{t('loginPage.title')}</h1>
      </div>
      <div className={s.formContainer}>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          style={{ width: '100%' }}
        >
          <Form.Item
            name="username"
            rules={[{ required: true, message: t('loginPage.usernameError') }]}
            label={t('loginPage.usernameTitle')}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder={t('loginPage.usernameTitle')}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true, message: t('loginPage.passwordError') }]}
            label={t('loginPage.passwordTitle')}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder={t('loginPage.passwordTitle')}
            />
          </Form.Item>
          <Form.Item wrapperCol={{ span: 16, offset: 8 }}>
            <Form.Item name="remember" valuePropName="checked" noStyle>
              <Checkbox>{t('loginPage.remember')}</Checkbox>
            </Form.Item>

            <Link to={ROUTER_PATH.FORGOT_PASSWORD} style={{ float: 'right' }}>
              {t('loginPage.forgotLink')}
            </Link>
          </Form.Item>

          <Form.Item wrapperCol={{ span: 16, offset: 8 }}>
            <>
              <Button loading={loading} type="primary" htmlType="submit" className="login-form-button">
                {t('loginPage.btnTitle')}
              </Button>
              {t('loginPage.or')} <Link to="/register">{t('loginPage.registerLink')}</Link>
            </>
          </Form.Item>
        </Form>
      </div>
    </div>
  )
}

export default Login
