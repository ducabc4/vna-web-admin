const vnResource = {
  translation: {
    layoutAuth: {
      title: 'Trang quản lý ứng dụng VietNam Airlines'
    },
    loginPage: {
      title: 'Đăng nhập',
      usernameTitle: 'Tên đăng nhập',
      usernameError: 'Tên đăng nhập không được để trống',
      passwordTitle: 'Mật khẩu',
      passwordError: 'Mật khẩu không được để trống',
      remember: 'Ghi nhớ đăng nhập',
      forgotLink: 'Quên mật khẩu',
      btnTitle: 'Đăng nhập',
      or: 'Hoặc',
      registerLink: 'Đăng ký ngay'
    },
    languageSelection: {
      vn: 'Tiếng Việt',
      en: 'Tiếng Anh'
    },
    forgotPasswordPage: {
      title: 'Quên mật khẩu',
      resetBy: 'Gửi mật khẩu mới qua',
      byMail: 'Email',
      byPhone: 'Số điện thoại',
      emailError: 'Email không được để trống',
      emailTitle: 'Email',
      phoneTitle: 'Số điện thoại',
      phoneError: 'Số điện thoại không được để trống',
      btnTitle: 'Gửi mật khẩu mới',
      backToLogin: 'Quay về trang đăng nhập'
    },
    sidebar: {
      userManagement: 'Quản lý người dùng',
      customerManagement: 'Quản lý khách hàng',
      walletReport: 'Báo cáo ví điện tử'
    },
    header: {
      btnLogout: 'Đăng xuất',
      namePrefix: 'Xin chào'
    }
  }
}

export default vnResource
