const enResources = {
  translation: {
    layoutAuth: {
      title: 'Vietnam Airlines administration page'
    },
    languageSelection: {
      vn: 'Vietnamese',
      en: 'English'
    },
    loginPage: {
      title: 'Login',
      usernameTitle: 'Username',
      usernameError: 'Please input your Username',
      passwordTitle: 'Password',
      passwordError: 'Please input your Password',
      remember: 'Remember Me',
      forgotLink: 'Forgot password',
      btnTitle: 'Login',
      or: 'Or',
      registerLink: 'Register now'
    },
    forgotPasswordPage: {
      title: 'Forgot Password',
      resetBy: 'Reset Password By',
      byMail: 'Email',
      byPhone: 'Phone Number',
      emailError: 'Please input your email',
      emailTitle: 'Email',
      phoneTitle: 'Phone Number',
      phoneError: 'Please input your phone number',
      btnTitle: 'Reset Password',
      backToLogin: 'Back to Login Page'
    },
    sidebar: {
      userManagement: 'User Management',
      customerManagement: 'Customer Management',
      walletReport: 'Wallet Report'
    },
    header: {
      btnLogout: 'Logout',
      namePrefix: 'Hello'
    },
    firstTimeLoginPage: {
      welcomeTitle: 'Welcome to Administration system',
      subTitle: 'Please change your password to start',
      newPasswordTitle: 'New Password',
      reEnterPasswordTitle: 'Re-enter new password',
      newPasswordError: 'Please input your new password',
      confirmPasswordError: 'Your passwords are not matched',
      btnUpdate: 'Update password',
      notifyChangePasswordSuccess: 'Your password has been changed succeed'
    }
  }
}

export default enResources
