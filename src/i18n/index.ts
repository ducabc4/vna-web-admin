import i18next from 'i18next'
import { initReactI18next } from 'react-i18next'
import LanguaDetector from 'i18next-browser-languagedetector'
import enResources from './resources/en'
import vnResource from './resources/vn'

i18next
  .use(LanguaDetector)
  .use(initReactI18next)
  .init({
    debug: true,
    fallbackLng: localStorage.getItem('i18n_lang') || 'vn',
    interpolation: {
      escapeValue: false
    },
    resources: {
      en: enResources,
      vn: vnResource
    }
  })

export default i18next
