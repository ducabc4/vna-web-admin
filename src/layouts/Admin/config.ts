import { IObject } from 'types/object'

export const siderStyle: IObject = {
  overflow: 'auto',
  height: '100vh',
  position: 'fixed',
  left: 0,
  top: 0,
  bottom: 0
}

export const headerStyle: IObject = {
  position: 'fixed',
  top: 0,
  right: 0,
  // width: 'calc(100% - 200px)',
  transition: 'all 0.2s',
  padding: 0,
  boxShadow: '4px 4px 40px 0 rgb(0 0 0 / 5%)'
}

export const contentStyle: IObject = {
  // marginLeft: 200,
  marginTop: 60
}
