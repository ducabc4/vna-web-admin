import {
  AppstoreOutlined,
  BarChartOutlined,
  CloudOutlined,
  PoweroffOutlined,
  ShopOutlined,
  TeamOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined
} from '@ant-design/icons'
import { Avatar, Button, Layout, Menu, MenuProps } from 'antd'
import { createElement, FC, useState } from 'react'

import { IProps } from './type'

import LanguageSelection from 'components/LanguageSelection'
import { useAppDispatch, useAppSelector } from 'hooks/rtk'
import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { ROUTER_NAME_LIST, ROUTER_PATH } from 'router/enum'
import { setCurrentUser } from 'store/slices/user'
import { contentStyle, headerStyle, siderStyle } from './config'
import s from './style.module.scss'

const { Header, Content, Sider } = Layout

const items: MenuProps['items'] = [
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  BarChartOutlined,
  CloudOutlined,
  AppstoreOutlined,
  TeamOutlined,
  ShopOutlined
].map((icon, index) => ({
  key: String(index + 1),
  icon: createElement(icon),
  label: `nav ${index + 1}`
}))

export const AdminLayout: FC<IProps> = ({ children }: IProps): JSX.Element => {
  const navigate = useNavigate()
  const { t } = useTranslation()
  const { currentUser } = useAppSelector(state => state.user)
  const dispatch = useAppDispatch()
  const [collapsed, setCollapsed] = useState(false)
  const headerWidth = useMemo(() => {
    return collapsed ? 'calc(100vw - 80px)' : 'calc(100vw - 200px)'
  }, [collapsed])
  const contentMarginLeft = useMemo(() => {
    return collapsed ? 80 : 200
  }, [collapsed])
  const menuItems: MenuProps['items'] = [
    {
      key: ROUTER_NAME_LIST.USER_MANAGEMENT,
      icon: createElement(UserOutlined),
      label: t('sidebar.userManagement') as string,
      title: t('sidebar.userManagement'),
      onClick: () => navigate(ROUTER_PATH.USER_MANAGEMENT)
    },
    {
      key: ROUTER_NAME_LIST.CUSTOMER_MANAGEMENT,
      icon: createElement(TeamOutlined),
      label: t('sidebar.customerManagement') as string,
      title: t('sidebar.customerManagement'),
      onClick: () => navigate(ROUTER_PATH.CUSTOMER_MANAGEMENT)
    },
    {
      key: ROUTER_NAME_LIST.WALLET_REPORTS,
      icon: createElement(BarChartOutlined),
      label: t('sidebar.walletReport'),
      onClick: () => navigate(ROUTER_PATH.WALLET_REPORTS),
      title: t('sidebar.walletReport')
    }
  ]

  const handleLogout = () => {
    localStorage.removeItem('token')
    dispatch(setCurrentUser(null))
    navigate(ROUTER_PATH.LOGIN)
  }

  return (
    <div className={s.layout}>
      <Layout hasSider>
        <Sider collapsible style={siderStyle} onCollapse={cls => setCollapsed(cls)}>
          <div className={s.logo} onClick={() => navigate(ROUTER_PATH.DASHBOARD)}>
            {collapsed ? (
              <img src="/vna_logo_small.png" alt="" height={30} />
            ) : (
              <img src="/VNA_logo_vn.webp" alt="" height={32} />
            )}
          </div>

          <Menu theme="dark" mode="inline" defaultSelectedKeys={[ROUTER_NAME_LIST.USER_MANAGEMENT]} items={menuItems} />
        </Sider>

        <Layout className="site-layout" style={contentStyle}>
          <Header
            // className={s.lightBg}
            style={{
              ...headerStyle,
              width: headerWidth
            }}
          >
            <div className={s.headerContainer}>
              <div className={s.featureContainer}>
                <div className={s.profileName}>
                  <Avatar
                    src="https://joeschmoe.io/api/v1/random"
                    style={{ width: 32, cursor: 'pointer', background: 'white' }}
                    onClick={() => navigate(ROUTER_PATH.PROFILE)}
                  />
                  <span style={{ marginLeft: 10, color: 'white' }}>
                    {t('header.namePrefix')}, {currentUser?.name ?? ''}
                  </span>
                </div>

                <LanguageSelection />

                <Button
                  type="primary"
                  icon={<PoweroffOutlined />}
                  // loading={loadings[2]}
                  onClick={handleLogout}
                  style={{ marginLeft: '-30px', marginRight: 10 }}
                  title={t('header.btnLogout')}
                />
              </div>
            </div>
          </Header>

          <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
            <div
              className={s.lightBg}
              style={{ padding: 24, textAlign: 'center', marginLeft: contentMarginLeft, transition: 'all 0.2s' }}
            >
              {children}
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  )
}
