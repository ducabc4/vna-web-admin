import React from 'react'

import { LAYOUT_NAME } from 'layouts/enum'
import { ROUTER_NAME_LIST } from 'router/enum'
import { IRouteConfigProps } from 'router/type'

export interface IRouteContext {
  routeConfig: IRouteConfigProps
}

const initialValues: IRouteContext = {
  routeConfig: {
    layout: LAYOUT_NAME.ADMIN,
    name: ROUTER_NAME_LIST.DASHBOARD
  }
}

export const RouteContext: React.Context<IRouteContext> = React.createContext(initialValues)
