import { Spin } from 'antd'
import { FC, Suspense, useCallback, useMemo } from 'react'
import { shallowEqual, useSelector } from 'react-redux'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'

import { IUseUserPermission, useCheckIsLogedIn, useUserPermission } from 'hooks/user'

import { IUser } from 'services/user/type'

import { HaveNotPermission } from 'components/HaveNotPermission'
import { IRouteConfigProps, IRouterItem } from 'router/type'

import { routers } from 'router'

import { AdminLayout } from './Admin'
import { AuthLayout } from './Auth'
import { BlankLayout } from './Blank'
import { LAYOUT_NAME } from './enum'
import { PageWrapper } from './PageWrapper'
import { IProps } from './type'

import { IStore } from 'store/type'
import s from './style.module.scss'

const LAYOUT: any = {
  [LAYOUT_NAME.ADMIN]: AdminLayout,
  [LAYOUT_NAME.AUTHEN]: AuthLayout,
  [LAYOUT_NAME.BLANK]: BlankLayout
}

const FallbackLoading: FC = (): JSX.Element => {
  return (
    <div className="fallback-loading">
      <Spin />
    </div>
  )
}

export const Layout: FC<IProps> = ({ children, ...props }): JSX.Element => {
  const currentRoute: IRouterItem | null = useSelector(
    (state: IStore): IRouterItem | null => state.router.current,
    shallowEqual
  )
  const user: IUser = useSelector((state: IStore): IUser => state.user.currentUser, shallowEqual)

  const permission: IUseUserPermission = useUserPermission()

  const layoutName: LAYOUT_NAME = useMemo((): LAYOUT_NAME => {
    return currentRoute?.layout || LAYOUT_NAME.BLANK
  }, [currentRoute])

  const CurrentLayout: any = LAYOUT[layoutName] || BlankLayout
  const isLoggedIn: boolean = useCheckIsLogedIn()

  const renderCurrentPage = useCallback((router: IRouterItem): JSX.Element => {
    const { isPrivate, component: Page } = router

    const havePermissionToAccess: boolean = true

    if (!havePermissionToAccess) {
      return <HaveNotPermission />
    }

    if ((isPrivate && isLoggedIn) || !isPrivate) {
      const routeConfig: IRouteConfigProps = {
        layout: router.layout,
        isPrivate: router.isPrivate,
        name: router.name
      }

      return <PageWrapper Page={Page} pageProps={{ ...props, routeConfig }} routeConfig={router} />
    }

    if (isPrivate && !isLoggedIn) {
      return <Navigate to="/login" replace />
    }

    return <div />
  }, [])

  return (
    <div className={s.layout}>
      <Suspense fallback={<FallbackLoading />}>
        <BrowserRouter>
          <CurrentLayout>
            <Routes>
              {routers.map((router: IRouterItem, index: number): JSX.Element => {
                return <Route key={index} path={router.path} element={renderCurrentPage(router)} />
              })}
            </Routes>
          </CurrentLayout>
        </BrowserRouter>
      </Suspense>
    </div>
  )
}
