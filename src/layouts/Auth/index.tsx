import { FC } from 'react'
import { IProps } from './type'

import LanguageSelection from 'components/LanguageSelection'

import s from './style.module.scss'
import { useTranslation } from 'react-i18next'

export const AuthLayout: FC<IProps> = ({ children }: IProps): JSX.Element => {
  const { t } = useTranslation()
  return (
    <div className={s.layout}>
      <div className={s.header}>
        <LanguageSelection />
      </div>
      <div className={s.content}>
        <div className={s.contentHeader}>
          <img src="/VNA_logo_vn.webp" alt="" />
          <h1>{t('layoutAuth.title')}</h1>
        </div>
        <div className={s.contentBody}>{children}</div>
      </div>
    </div>
  )
}
