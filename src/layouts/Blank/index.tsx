import { FC } from 'react'
import { IProps } from './type'

import s from './style.module.scss'

export const BlankLayout: FC<IProps> = ({ children }: IProps): JSX.Element => {
  return (
    <div className={s.BlankLayout}>
      <div>{children}</div>
    </div>
  )
}
