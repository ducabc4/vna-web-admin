import { BackgroundTasks } from 'components/BackgroundTasks'
import { Layout } from 'layouts'
import { ConfigProvider } from 'antd'

import 'scss/index.scss'
import s from './style.module.scss'

function App(): JSX.Element {
  return (
    <div className={s.app}>
      <ConfigProvider>
        <BackgroundTasks />
        <Layout />
      </ConfigProvider>
    </div>
  )
}

export default App
