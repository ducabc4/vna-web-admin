import { Layout } from 'antd'
import s from './header.module.scss'

const Header = () => {
  return (
    <Layout.Header>
      <div className={s.headerContainer}></div>
    </Layout.Header>
  )
}

export default Header
