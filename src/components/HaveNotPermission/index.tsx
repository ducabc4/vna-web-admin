import { FC } from 'react'

export const HaveNotPermission: FC = (): JSX.Element => {
  return <div>You did not have permission to access this resource.</div>
}
