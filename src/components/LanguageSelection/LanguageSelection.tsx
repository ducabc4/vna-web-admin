import { Select } from 'antd'
import { useTranslation } from 'react-i18next'

const LanguageSelection = () => {
  const { t, i18n } = useTranslation()
  const handleChangeLang = (lang: string) => {
    const newLang = lang

    i18n.changeLanguage(newLang)
    localStorage.setItem('i18n_lang', newLang)
  }
  return (
    <Select defaultValue={i18n.resolvedLanguage} style={{ marginRight: 40 }} onChange={handleChangeLang}>
      <Select.Option value="vn">{t('languageSelection.vn')}</Select.Option>
      <Select.Option value="en">{t('languageSelection.en')}</Select.Option>
    </Select>
  )
}

export default LanguageSelection
