import { lazy } from 'react'

import { LAYOUT_NAME } from 'layouts/enum'

import { ROUTER_NAME_LIST, ROUTER_PATH } from './enum'
import { IRouterItem } from './type'

export const routers: IRouterItem[] = [
  {
    path: ROUTER_PATH.DASHBOARD,
    component: lazy((): Promise<any> => import('pages/Dashboard')),
    name: ROUTER_NAME_LIST.DASHBOARD,
    layout: LAYOUT_NAME.ADMIN,
    isPrivate: true
  },
  {
    path: ROUTER_PATH.LOGIN,
    component: lazy((): Promise<any> => import('pages/Login')),
    name: ROUTER_NAME_LIST.LOGIN,
    layout: LAYOUT_NAME.AUTHEN
  },
  {
    path: ROUTER_PATH.FIRST_TIME_LOGIN,
    component: lazy((): Promise<any> => import('pages/FirstTimeLogin')),
    name: ROUTER_NAME_LIST.FIRST_TIME_LOGIN,
    layout: LAYOUT_NAME.AUTHEN
  },
  {
    path: ROUTER_PATH.FORGOT_PASSWORD,
    component: lazy((): Promise<any> => import('pages/ForgotPassword')),
    name: ROUTER_NAME_LIST.FORGOT_PASSWORD,
    layout: LAYOUT_NAME.AUTHEN
  },
  {
    path: ROUTER_PATH.USER_MANAGEMENT,
    component: lazy((): Promise<any> => import('pages/User')),
    name: ROUTER_NAME_LIST.USER_MANAGEMENT,
    layout: LAYOUT_NAME.ADMIN
  },
  {
    path: ROUTER_PATH.CUSTOMER_MANAGEMENT,
    component: lazy((): Promise<any> => import('pages/Customers')),
    name: ROUTER_NAME_LIST.CUSTOMER_MANAGEMENT,
    layout: LAYOUT_NAME.ADMIN
  },
  {
    path: ROUTER_PATH.WALLET_REPORTS,
    component: lazy((): Promise<any> => import('pages/Reports')),
    name: ROUTER_NAME_LIST.WALLET_REPORTS,
    layout: LAYOUT_NAME.ADMIN
  },
  {
    path: ROUTER_PATH.PROFILE,
    component: lazy((): Promise<any> => import('pages/Profile')),
    name: ROUTER_NAME_LIST.PROFILE,
    layout: LAYOUT_NAME.ADMIN
  },
  {
    path: '*',
    component: lazy((): Promise<any> => import('pages/PageNotFound')),
    name: ROUTER_NAME_LIST.PAGE_NOT_FOUND,
    layout: LAYOUT_NAME.BLANK
  }
]
