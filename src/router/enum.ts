export enum ROUTER_NAME_LIST {
  DASHBOARD = 'DASHBOARD',
  LOGIN = 'LOGIN',
  FIRST_TIME_LOGIN = 'FIRST_TIME_LOGIN',
  FORGOT_PASSWORD = 'FORGOT_PASSWORD',
  PAGE_NOT_FOUND = 'PAGE_NOT_FOUND',
  USER_MANAGEMENT = 'USER_MANAGEMENT',
  CUSTOMER_MANAGEMENT = 'CUSTOMER_MANAGEMENT',
  WALLET_TRANSACTIONS = 'WALLET_TRANSACTION',
  WALLET_REPORTS = 'WALLET_REPORTS',
  CREDIT_CARDS = 'CREDIT_CARDS',
  CREDIT_CARD_TRANSACTION = 'CREDIT_CARD_TRANSACTION',
  LOYALTY_MANAGEMENT = 'LOYALTY_MANAGEMENT',
  PROFILE = 'PROFILE'
}

export enum ROUTER_PATH {
  DASHBOARD = '/',
  LOGIN = '/login',
  FIRST_TIME_LOGIN = '/first-time-login',
  FORGOT_PASSWORD = '/forgot-password',
  PAGE_NOT_FOUND = '*',
  USER_MANAGEMENT = '/users',
  CUSTOMER_MANAGEMENT = '/customers',
  PROFILE = '/profile',
  WALLET_REPORTS = '/wallet-reports'
  // WALLET_TRANSACTION = '/customers'
}
